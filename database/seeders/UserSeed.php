<?php

namespace Database\Seeders;

use App\Helpers\Constants;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Kasun Dulanjana',
            'email' => 'kasun.dulanjana@ymail.com',
            'password' => bcrypt('123456'),
            'type_id' => Constants::USER_TYPES['admin'],
        ]);

        User::create([
            'name' => 'Kasun Dulanjana',
            'email' => 'mkdulanjana@gmail.com',
            'password' => bcrypt('123456'),
            'type_id' => Constants::USER_TYPES['user'],
        ]);
    }
}
