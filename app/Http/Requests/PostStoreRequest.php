<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:100|min:2',
            'slug' => 'required|string|max:100|min:2|unique:posts,slug',
            'body' => 'required|string|min:50',
            'image' => 'nullable|image|mimes:jpg,png'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'The post title field is required.',
            'title.string' => 'The post title must be a string.',
            'title.max' => 'The post title must not be greater than :max characters.',
            'title.min' => 'The post title must be at least :min characters.',
            'slug.required' => 'The post slug field is required.',
            'slug.string' => 'The post slug must be a string.',
            'slug.max' => 'The post slug must not be greater than :max characters.',
            'slug.min' => 'The post slug must be at least :min characters.',
            'slug.unique' => 'The post slug has already been taken.',
            'body.required' => 'The post body field is required.',
            'body.string' => 'The post body must be a string.',
            'body.min' => 'The post body must be at least :min characters.',
            'image.image' => 'The post image must be an image.',
            'image.mimes' => 'The post image must be a file of type: :values.'
        ];
    }
}
