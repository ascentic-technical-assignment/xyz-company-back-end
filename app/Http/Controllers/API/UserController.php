<?php

namespace App\Http\Controllers\API;

use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegistrationRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /**
     * This function returns the user token once the user get authenticated successfully.
     * @param UserLoginRequest $oRequest
     * @return JsonResponse
     */
    public function performUserLogin(UserLoginRequest $oRequest): JsonResponse
    {
        $aUserLoginResponse = [
            'status' => false,
            'message' => 'User registration failed',
            'token' => '',
            'user_id' => null,
            'user_type' => null
        ];
        $iStatusCode = Response::HTTP_OK;
        try {
            $oUser = User::where([
                'email' => $oRequest->get('email')
            ])->first();
            if ($oUser !== null) {
                if (Auth::attempt([
                    'email' => $oRequest->get('email'),
                    'password' => $oRequest->get('password')
                ])) {
                    $aUserLoginResponse['status'] = true;
                    $aUserLoginResponse['message'] = 'User login succeeded';
                    $aUserLoginResponse['token'] = $oUser->createToken('Auth Token')->plainTextToken;
                    $aUserLoginResponse['user_id'] = $oUser->id;
                    $aUserLoginResponse['user_type'] = array_search($oUser->type_id, Constants::USER_TYPES, true);
                } else {
                    $aUserLoginResponse['message'] = "Invalid credentials";
                }
            } else {
                $aUserLoginResponse['message'] = "Invalid credentials";
            }
        } catch (Exception $oException) {
            Log::error($oException->getMessage() . ' - ' . $oException->getTraceAsString());
            $aUserLoginResponse['message'] = 'Something went wrong. We are working on this to get back';
            $iStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return response()->json($aUserLoginResponse, $iStatusCode);
    }

    /**
     * This function returns the user token once a user get registered successfully.
     * @param UserRegistrationRequest $oRequest
     * @return JsonResponse
     */
    public function performUserRegistration(UserRegistrationRequest $oRequest): JsonResponse
    {
        $aUserRegistrationResponse = [
            'status' => false,
            'message' => 'User registration failed',
            'token' => '',
            'user_id' => null,
            'user_type' => null
        ];
        $iStatusCode = Response::HTTP_OK;
        try {
            $oUser = User::getInstance();
            $oUser->name = $oRequest->get('name');
            $oUser->email = $oRequest->get('email');
            $oUser->password = bcrypt($oRequest->get('confirm_password'));
            $oUser->save();
            $aUserRegistrationResponse['status'] = true;
            $aUserRegistrationResponse['message'] = 'User registration succeeded';
            $aUserRegistrationResponse['token'] = $oUser->createToken('Auth Token')->plainTextToken;
            $aUserRegistrationResponse['user_id'] = $oUser->id;
            $aUserRegistrationResponse['user_type'] = array_search($oUser->type_id, Constants::USER_TYPES, true);
        } catch (Exception $oException) {
            Log::error($oException->getMessage() . ' - ' . $oException->getTraceAsString());
            $aUserRegistrationResponse['message'] = 'Something went wrong. We are working on this to get back';
            $iStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return response()->json($aUserRegistrationResponse, $iStatusCode);
    }
}
