<?php

namespace App\Http\Controllers\API;

use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostApproveRequest;
use App\Http\Requests\PostDeleteRequest;
use App\Http\Requests\PostRetrieveRequest;
use App\Http\Requests\PostStoreRequest;
use App\Models\Post;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class PostController extends Controller
{
    /**
     * This function returns all the posts with their basic details for the forum's posts listing page.
     * @return JsonResponse
     */
    public function getPosts(): JsonResponse
    {
        $aPosts = [
            'status' => false,
            'data' => [],
            'message' => 'Posts retrieving failed'
        ];
        $iStatusCode = Response::HTTP_OK;
        try {
            $oPosts = Post::with('user')->where('is_approved', '=', true)->latest()->get();
            if ($oPosts->isNotEmpty()) {
                foreach ($oPosts as $oPost) {
                    $aPosts['data'][] = [
                        'id' => $oPost->id,
                        'user' => $oPost->user->name,
                        'user_id' => $oPost->user_id,
                        'slug' => $oPost->slug,
                        'title' => $oPost->title,
                        'body' => Str::limit($oPost->body, 50),
                        'image' => $oPost->image !== null ? asset('images/posts/' . $oPost->image) : asset('images/posts/default_thumbnail.png'),
                        'created_at' => date('Y-m-d h:i A', strtotime($oPost->created_at))
                    ];
                }
                $aPosts['status'] = true;
                $aPosts['message'] = '';
            } else {
                $aPosts['message'] = 'No posts found';
            }
        } catch (Exception $oException) {
            Log::error($oException->getMessage() . ' - ' . $oException->getTraceAsString());
            $aPosts['message'] = 'Something went wrong. We are working on this to get back';
            $iStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return response()->json($aPosts, $iStatusCode);
    }

    /**
     * This function returns all the pending posts with their basic details for the forum's pending posts listing page.
     * @return JsonResponse
     */
    public function getPendingPosts(): JsonResponse
    {
        $aPendingPosts = [
            'status' => false,
            'data' => [],
            'message' => 'Pending posts retrieving failed'
        ];
        $iStatusCode = Response::HTTP_OK;
        try {
            if (auth()->user()->type_id === Constants::USER_TYPES['admin']) {
                $oPosts = Post::with('user')->where([
                    'is_approved' => false
                ])->latest()->get();
                if ($oPosts->isNotEmpty()) {
                    foreach ($oPosts as $oPost) {
                        $aPendingPosts['data'][] = [
                            'id' => $oPost->id,
                            'user' => $oPost->user->name,
                            'slug' => $oPost->slug,
                            'title' => $oPost->title,
                            'body' => Str::limit($oPost->body, 50),
                            'image' => $oPost->image !== null ? asset('images/posts/' . $oPost->image) : asset('images/posts/default_thumbnail.png'),
                            'created_at' => date('Y-m-d h:i A', strtotime($oPost->created_at))
                        ];
                    }
                    $aPendingPosts['status'] = true;
                    $aPendingPosts['message'] = '';
                } else {
                    $aPendingPosts['message'] = 'No pending posts found';
                }
            } else {
                $aPendingPosts['message'] = 'You are not permitted to view pending posts';
                $iStatusCode = Response::HTTP_UNAUTHORIZED;
            }
        } catch (Exception $oException) {
            Log::error($oException->getMessage() . ' - ' . $oException->getTraceAsString());
            $aPendingPosts['message'] = 'Something went wrong. We are working on this to get back';
            $iStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return response()->json($aPendingPosts, $iStatusCode);
    }

    /**
     * This function returns all the relevant details of the given post slug for the forum's post details page.
     * @param PostRetrieveRequest $oRequest
     * @return JsonResponse
     */
    public function getPostDetails(PostRetrieveRequest $oRequest): JsonResponse
    {
        $aPost = [
            'data' => [],
            'message' => 'Post retrieving failed'
        ];
        $iStatusCode = Response::HTTP_OK;
        try {
            $oPost = Post::with('user')->where([
                'slug' => $oRequest->get('slug')
            ])->first();
            if ($oPost !== null) {
                if (!$oPost->is_approved && auth()->user()->type_id === Constants::USER_TYPES['user']) {
                    $aPost['message'] = 'You are not permitted to view a pending post';
                    $iStatusCode = Response::HTTP_UNAUTHORIZED;
                } else {
                    $aPost['data'] = [
                        'id' => $oPost->id,
                        'user' => $oPost->user->name,
                        'slug' => $oPost->slug,
                        'title' => $oPost->title,
                        'body' => $oPost->body,
                        'image' => $oPost->image !== null ? asset('images/posts/' . $oPost->image) : asset('images/posts/default_large.png'),
                        'created_at' => date('Y-m-d h:i A', strtotime($oPost->created_at))
                    ];
                }
            } else {
                $aPost['message'] = 'Cannot find the post with the given slug';
                $iStatusCode = Response::HTTP_NOT_FOUND;
            }
        } catch (Exception $oException) {
            Log::error($oException->getMessage() . ' - ' . $oException->getTraceAsString());
            $aPost['message'] = 'Something went wrong. We are working on this to get back';
            $iStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return response()->json($aPost, $iStatusCode);
    }

    /**
     * This function returns the response of the post storing process from the user submitted data.
     * @param PostStoreRequest $oRequest
     * @return JsonResponse
     */
    public function storePost(PostStoreRequest $oRequest): JsonResponse
    {
        $aPostStoreResponse = [
            'status' => false,
            'message' => 'Post saving failed'
        ];
        $iStatusCode = Response::HTTP_OK;
        try {
            $oPost = Post::getInstance();
            $oPost->user_id = auth()->user()->id;
            $oPost->title = $oRequest->get('title');
            $oPost->slug = $oRequest->get('slug');
            $oPost->body = $oRequest->get('body');
            $oPost->image = $oRequest->get('image') !== null && $oRequest->get('image') !== '' ? Str::random(10) : null;
            $oPost->is_approved = auth()->user()->type_id === Constants::USER_TYPES['admin'];
            $oPost->save();
            $aPostStoreResponse['status'] = true;
            $aPostStoreResponse['message'] = 'Post saving succeeded';
        } catch (Exception $oException) {
            Log::error($oException->getMessage() . ' - ' . $oException->getTraceAsString());
            $aPostStoreResponse['message'] = 'Something went wrong. We are working on this to get back';
            $iStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return response()->json($aPostStoreResponse, $iStatusCode);
    }

    /**
     * This function returns the response of the post deleting process of the given slug by the user.
     * @param PostDeleteRequest $oRequest
     * @return JsonResponse
     */
    public function deletePost(PostDeleteRequest $oRequest): JsonResponse
    {
        $aPostDeleteResponse = [
            'status' => false,
            'message' => 'Post deleting failed'
        ];
        $iStatusCode = Response::HTTP_OK;
        try {
            $aWhere = [
                'slug' => $oRequest->get('slug')
            ];
            if (auth()->user()->type_id !== Constants::USER_TYPES['admin']) {
                $aWhere['user_id'] = auth()->user()->id;
            }
            $oPost = Post::where($aWhere)->first();
            if ($oPost !== null) {
                $oPost->delete();
                $aPostDeleteResponse['status'] = true;
                $aPostDeleteResponse['message'] = 'Post deleting succeeded';
            } else {
                $aPostDeleteResponse['message'] = 'Cannot find the post with the given id';
                $iStatusCode = Response::HTTP_NOT_FOUND;
            }
        } catch (Exception $oException) {
            Log::error($oException->getMessage() . ' - ' . $oException->getTraceAsString());
            $aPostDeleteResponse['message'] = 'Something went wrong. We are working on this to get back';
            $iStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return response()->json($aPostDeleteResponse, $iStatusCode);
    }

    /**
     * This function returns the response of the post approving process of admin on a new post.
     * @param PostApproveRequest $oRequest
     * @return JsonResponse
     */
    public function approvePost(PostApproveRequest $oRequest): JsonResponse
    {
        $aPostApproveResponse = [
            'status' => false,
            'message' => 'Post approving failed'
        ];
        $iStatusCode = Response::HTTP_OK;
        try {
            if (auth()->user()->type_id === Constants::USER_TYPES['admin']) {
                $oPost = Post::where([
                    'slug' => $oRequest->get('slug')
                ])->first();
                if ($oPost !== null) {
                    $oPost->is_approved = true;
                    $oPost->save();
                    $aPostApproveResponse['status'] = true;
                    $aPostApproveResponse['message'] = 'Post approving succeeded';
                } else {
                    $aPostApproveResponse['message'] = 'Cannot find the post with the given slug';
                    $iStatusCode = Response::HTTP_NOT_FOUND;
                }
            } else {
                $aPostApproveResponse['message'] = 'You are not permitted to perform this task';
                $iStatusCode = Response::HTTP_UNAUTHORIZED;
            }
        } catch (Exception $oException) {
            Log::error($oException->getMessage() . ' - ' . $oException->getTraceAsString());
            $aPostApproveResponse['message'] = 'Something went wrong. We are working on this to get back';
            $iStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return response()->json($aPostApproveResponse, $iStatusCode);
    }
}
