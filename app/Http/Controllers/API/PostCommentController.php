<?php

namespace App\Http\Controllers\API;

use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostCommentsRetrieveRequest;
use App\Http\Requests\PostCommentStoreRequest;
use App\Models\Post;
use App\Models\PostComment;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class PostCommentController extends Controller
{
    /**
     * This function returns all the comments for the given post id.
     * @param PostCommentsRetrieveRequest $oRequest
     * @return JsonResponse
     */
    public function getPostComments(PostCommentsRetrieveRequest $oRequest): JsonResponse
    {
        $aPostComments = [
            'status' => false,
            'message' => 'Comments loading failed',
            'data' => []
        ];
        $iStatusCode = Response::HTTP_OK;
        try {
            if (auth()->user()->type_id === Constants::USER_TYPES['admin']) {
                $oPostComments = PostComment::with('user', 'post')
                    ->whereRelation('post', 'slug', '=', $oRequest->get('slug'))
                    ->latest()->get();
            } else {
                $oPostComments = PostComment::with('user', 'post')
                    ->whereRelation('post', 'is_approved', '=', true)
                    ->whereRelation('post', 'slug', '=', $oRequest->get('slug'))
                    ->latest()->get();
            }
            if ($oPostComments->isNotEmpty()) {
                $aPostComments['status'] = true;
                $aPostComments['message'] = 'Comments loading succeeded';
                foreach ($oPostComments as $oPostComment) {
                    $aPostComments['data'][] = [
                        'id' => $oPostComment->id,
                        'user' => $oPostComment->user->name,
                        'comment' => $oPostComment->comment,
                        'created_at' => date('Y-m-d h:iA', strtotime($oPostComment->created_at))
                    ];
                }
            } else {
                $aPostComments['message'] = 'No comments found';
            }
        } catch (Exception $oException) {
            Log::error($oException->getMessage() . ' - ' . $oException->getTraceAsString());
            $iStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return response()->json($aPostComments, $iStatusCode);
    }

    /**
     * This function returns the response of the comment storing process from the user submitted data.
     * @param PostCommentStoreRequest $oRequest
     * @return JsonResponse
     */
    public function storePostComment(PostCommentStoreRequest $oRequest): JsonResponse
    {
        $aPostCommentStoreResponse = [
            'status' => false,
            'message' => 'Post commenting failed'
        ];
        $iStatusCode = Response::HTTP_OK;
        try {
            if (auth()->user()->type_id === Constants::USER_TYPES['admin']) {
                $oPost = Post::where([
                    'slug' => $oRequest->get('slug')
                ])->first();
            } else {
                $oPost = Post::where([
                    'slug' => $oRequest->get('slug'),
                    'is_approved' => true
                ])->first();
            }
            if ($oPost !== null) {
                $oPostComment = PostComment::getInstance();
                $oPostComment->post_id = $oPost->id;
                $oPostComment->user_id = auth()->user()->id;
                $oPostComment->comment = $oRequest->get('comment');
                $oPostComment->save();
                $aPostCommentStoreResponse['status'] = true;
                $aPostCommentStoreResponse['message'] = 'Post commenting succeeded';
            } else {
                $aPostCommentStoreResponse['message'] = 'Cannot find the post with the given id';
            }
        } catch (Exception $oException) {
            Log::error($oException->getMessage() . ' - ' . $oException->getTraceAsString());
            $iStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return response()->json($aPostCommentStoreResponse, $iStatusCode);
    }
}
