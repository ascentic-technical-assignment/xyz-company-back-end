<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Post extends Model
{
    use HasFactory;

    private static $oInstance;

    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'title',
        'slug',
        'body',
        'image',
        'is_approved'
    ];

    /**
     * @return Post
     */
    public static function getInstance(): Post
    {
        if (!isset(self::$oInstance)) {
            self::$oInstance = new self();
        }
        return self::$oInstance;
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function postComments(): HasMany
    {
        return $this->hasMany(PostComment::class);
    }
}
