<?php

namespace App\Helpers;

class Constants
{
    public const USER_TYPES = [
        'admin' => 1,
        'user' => 2
    ];
}
