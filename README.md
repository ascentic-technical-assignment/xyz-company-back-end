# xyz-company-back-end

## Project setup
```
1. git clone
2. composer install
3. Setup mysql DB in .env file
4. Run php artisan migrate (I have entered some dummy data for users and posts. 
If you need to run dummy data please run php artisan migrate --seed)
5. php artisan serve
```
### Special Note
```
1. Please make to run the API on http://127.0.0.1:8000 because it is hardcoded 
on the front-end application
```
