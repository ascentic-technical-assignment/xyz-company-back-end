<?php

use App\Http\Controllers\API\PostCommentController;
use App\Http\Controllers\API\PostController;
use App\Http\Controllers\API\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [UserController::class, 'performUserLogin'])->name('user-login');
Route::post('/register', [UserController::class, 'performUserRegistration'])->name('user-registration');

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('/post/list', [PostController::class, 'getPosts'])->name('post-list');
    Route::get('/post/pending/list', [PostController::class, 'getPendingPosts'])->name('pending-post-list');
    Route::get('/post/details', [PostController::class, 'getPostDetails'])->name('post-details');
    Route::post('/post/create', [PostController::class, 'storePost'])->name('post-create');
    Route::delete('/post/delete', [PostController::class, 'deletePost'])->name('post-delete');
    Route::put('/post/approve', [PostController::class, 'approvePost'])->name('post-approve');

    Route::get('/post/comment/list', [PostCommentController::class, 'getPostComments'])->name('post-comment-list');
    Route::post('/post/comment/create', [PostCommentController::class, 'storePostComment'])->name('post-comment-create');
});
